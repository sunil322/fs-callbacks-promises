const fs = require('fs');
const path = require('path');

const createDirectory = (directoryName) => {

    const directory = path.join(__dirname, directoryName);

    return new Promise((reslove, reject) => {
        fs.mkdir(directory, { recursive: true }, (error) => {
            if (error) {
                reject(error);
            } else {
                reslove(directory);
            }
        });
    });
};

const createRandomJsonFiles = (directory) => {

    let noOfFiles = Math.ceil(Math.random()*10);

    return new Promise((reslove,reject)=>{
        function createFile() {
            if (noOfFiles >= 1) {
                let randomFileName = Math.random().toString().slice(2) + '.json';
                const jsonFilePath = path.join(directory, randomFileName);
                fs.open(jsonFilePath, 'w', (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        console.log(randomFileName + " file created");
                        noOfFiles--;
                        createFile();
                        if (noOfFiles === 0) {
                            reslove(directory);
                        }
                    }
                });
            }
        }
        createFile();
    });
};

const deleteJsonFiles = (directory) => {

    return new Promise((reslove,reject)=>{
        fs.readdir(directory, (error, data) => {
            if (error) {
                reject(error);
            } else {
                let noOfFilesPresent = data.length;
                function deleteFile() {
                    if (noOfFilesPresent >= 1) {
                        const fileName = data[noOfFilesPresent - 1];
                        const filePath = path.join(directory, fileName);
                        fs.unlink(filePath, (error) => {
                            if (error) {
                                reject(error);
                            } else {
                                console.log(fileName + " Deleted");
                                noOfFilesPresent--;
                                deleteFile();
                                if (noOfFilesPresent === 0) {
                                    reslove("Files creation and deletion tasks completed");
                                }
                            }
                        });
                    }
                }
                deleteFile();
            }
        });
    });
};

module.exports = { createDirectory, createRandomJsonFiles, deleteJsonFiles };