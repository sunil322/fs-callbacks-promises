const fs = require('fs');
const path = require('path');

//Reading lipsum.txt file
const readingFile = () => {

    return new Promise((resolve, reject) => {
        fs.readFile('lipsum.txt', 'utf8', (error, data) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
};

//Writing upper case data to a file and storing that file name in filenames.txt
const writeUpperCaseData = (data) => {

    const upperCaseFilePath = path.join(__dirname, 'lipsumUpper.txt');
    const fileName = path.join(__dirname, 'filenames.txt');

    let dataInUpperCase = data.toUpperCase();

    return new Promise((resolve, reject) => {
        fs.writeFile(upperCaseFilePath, dataInUpperCase, (error) => {
            if (error) {
                reject(error);
            } else {
                fs.writeFile(fileName, upperCaseFilePath, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(upperCaseFilePath);
                    }
                });
            }
        });
    });
};

//Writing lower case data to a file and storing that file name in filenames.txt
const writeLowerCaseData = (upperCaseFilePath) => {

    return new Promise((resolve, reject) => {

        fs.readFile(upperCaseFilePath, 'utf8', (error, data) => {
            if (error) {
                reject(error);
            } else {
                const lowerCaseFilePath = path.join(__dirname, 'lipsumLower.txt');
                const fileName = path.join(__dirname, 'filenames.txt');

                let dataInLowerCase = data.toLowerCase();
                let dataInSentences = dataInLowerCase.split('.').join('\n');

                fs.writeFile(lowerCaseFilePath, dataInSentences, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        fs.writeFile(fileName, '\n' + lowerCaseFilePath, { flag: 'a' }, (error) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve({ upperCaseFilePath, lowerCaseFilePath });
                            }
                        });
                    }
                });
            }
        });
    });
};

//Writing sorted data to a file and storing that file name in filenames.txt
const writeSortedData = (upperCaseFilePath, lowerCaseFilePath) => {

    return new Promise((resolve, reject) => {
        fs.readFile(upperCaseFilePath, 'utf8', (error, dataInUpper) => {
            if (error) {
                reject(error);
            } else {
                fs.readFile(lowerCaseFilePath, 'utf8', (error, dataInLower) => {
                    if (error) {
                        reject(error);
                    } else {
                        const sortedDataFilePath = path.join(__dirname, 'lipsumSorted.txt');
                        const filesName = path.join(__dirname, 'filenames.txt');

                        let totalData = dataInLower + dataInUpper;
                        let sortedData = totalData.split(' ').sort().join('\n');

                        fs.writeFile(sortedDataFilePath, sortedData, (error) => {
                            if (error) {
                                reject(error);
                            } else {
                                fs.writeFile(filesName, '\n' + sortedDataFilePath, { flag: 'a' }, (error) => {
                                    if (error) {
                                        reject(error);
                                    } else {
                                        resolve(filesName);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    });
};

//Delete all files listed in the filenames.txt
const readFileDeleteListedFiles = (fileName) => {

    return new Promise((resolve, reject) => {
        fs.readFile(fileName, 'utf8', (error, data) => {
            if (error) {
                reject(error);
            } else {

                let noOfFilesPresent = data.split('\n').length;
                let filesList = data.split('\n');

                function deleteFile() {
                    if (noOfFilesPresent >= 1) {
                        let filePath = filesList[noOfFilesPresent - 1];
                        fs.unlink(filePath, (error) => {
                            if (error) {
                                reject(error);
                            } else {
                                noOfFilesPresent--;
                                deleteFile();
                                if (noOfFilesPresent === 0) {
                                    resolve("All files deleted successfully");
                                }
                            }
                        });
                    }
                }
                deleteFile();
            }
        });
    });
};

module.exports = { readingFile, writeUpperCaseData, writeLowerCaseData, writeSortedData, readFileDeleteListedFiles };