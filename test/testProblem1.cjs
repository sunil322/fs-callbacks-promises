const {createDirectory,createRandomJsonFiles,deleteJsonFiles} = require('../problem1.cjs');

createDirectory('JsonFolder')
.then((directory)=>{
    return createRandomJsonFiles(directory);
})
.then((directory)=>{
    return deleteJsonFiles(directory);
})
.then((finalResponse)=>{
    console.log(finalResponse);
})
.catch((error)=>{
    console.log(error);
});