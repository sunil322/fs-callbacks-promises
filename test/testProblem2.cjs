const {readingFile,writeUpperCaseData,writeLowerCaseData,writeSortedData,readFileDeleteListedFiles} = require('../problem2.cjs');

readingFile()
.then((data)=>{
    return writeUpperCaseData(data);
})
.then((upperCaseFilePath)=>{
    return writeLowerCaseData(upperCaseFilePath);
})
.then((filePath)=>{
    return writeSortedData(filePath.upperCaseFilePath,filePath.lowerCaseFilePath);
})
.then((fileName)=>{
    return readFileDeleteListedFiles(fileName);
})
.then((finalResponse)=>{
    console.log(finalResponse);
})
.catch((error)=>{
    console.log(error);
});
